# Train Stations
Pallavi Gadgil  
October 4, 2017  



## R Markdown
Train Stations near me



```r
library(leaflet)


railIcon <- makeIcon(
  iconUrl = "http://www.njtransit.com/images/rg_rail_icon_sm.jpg",
  iconWidth = 40*215/230, iconHeight = 40,
  iconAnchorX = 40*215/230/2, iconAnchorY = 16
)

railGeo <- data.frame(
  lat = c(40.56808, 40.518444, 40.54075),
  lng = c(-74.329795, -74.412419, -74.360339))
railSites <- c(
   "<a href='http://www.njtransit.com/rg/rg_servlet.srv?hdnPageAction=TrainStationLookupFrom&selStation=83'>Metro Park Station</a>",
  "<a href='http://www.njtransit.com/rg/rg_servlet.srv?hdnPageAction=TrainStationLookupFrom&selStation=38/'>Edison Station</a>",
  "<a href='http://www.njtransit.com/rg/rg_servlet.srv?hdnPageAction=TrainStationLookupFrom&selStation=84'>Metuchen Station</a>"
)
railLabels <- c("Metro Park Stattion","Edison Station", "Metuchen Station")
railGeo  %>% 
  leaflet() %>%
  addTiles() %>%
  addMarkers(icon = railIcon,popup=railLabels)
```

<!--html_preserve--><div id="htmlwidget-b1f6b23059480c50bdc5" style="width:672px;height:480px;" class="leaflet html-widget"></div>
<script type="application/json" data-for="htmlwidget-b1f6b23059480c50bdc5">{"x":{"options":{"crs":{"crsClass":"L.CRS.EPSG3857","code":null,"proj4def":null,"projectedBounds":null,"options":{}}},"calls":[{"method":"addTiles","args":["https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",null,null,{"minZoom":0,"maxZoom":18,"maxNativeZoom":null,"tileSize":256,"subdomains":"abc","errorTileUrl":"","tms":false,"continuousWorld":false,"noWrap":false,"zoomOffset":0,"zoomReverse":false,"opacity":1,"zIndex":null,"unloadInvisibleTiles":null,"updateWhenIdle":null,"detectRetina":false,"reuseTiles":false,"attribution":"&copy; <a href=\"http://openstreetmap.org\">OpenStreetMap<\/a> contributors, <a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA<\/a>"}]},{"method":"addMarkers","args":[[40.56808,40.518444,40.54075],[-74.329795,-74.412419,-74.360339],{"iconUrl":{"data":"http://www.njtransit.com/images/rg_rail_icon_sm.jpg","index":0},"iconWidth":37.3913043478261,"iconHeight":40,"iconAnchorX":18.695652173913,"iconAnchorY":16},null,null,{"clickable":true,"draggable":false,"keyboard":true,"title":"","alt":"","zIndexOffset":0,"opacity":1,"riseOnHover":false,"riseOffset":250},["Metro Park Stattion","Edison Station","Metuchen Station"],null,null,null,null,null,null]}],"limits":{"lat":[40.518444,40.56808],"lng":[-74.412419,-74.329795]}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

```r
railGeo
```

```
##        lat       lng
## 1 40.56808 -74.32980
## 2 40.51844 -74.41242
## 3 40.54075 -74.36034
```

